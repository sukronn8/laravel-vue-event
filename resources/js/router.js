import VueRouter from 'vue-router'

// Pages
import Home from './pages/Home'
import Register from './pages/Register'
import UserRegister from './pages/user/Register'
import UserLogin from './pages/user/Login'
import UserDashboard from './pages/user/Dashboard'
import UserJoined from './pages/user/Joined'
import OrganizationRegister from './pages/organization/Register'
import OrganizationLogin from './pages/organization/Login'
import OrganizationDashboard from './pages/organization/Dashboard'
import OrganizationCreate from './pages/organization/Create'


// Routes
const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/user-register',
        name: 'user.register',
        component: UserRegister,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/user-login',
        name: 'user.login',
        component: UserLogin,
        meta: {
            auth: undefined
        }
    },
    // USER ROUTES
    {
        path: '/user/dashboard',
        name: 'user.dashboard',
        component: UserDashboard,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/user/dashboard/joined',
        name: 'user.joined',
        component: UserJoined,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/organization-register',
        name: 'organization.register',
        component: OrganizationRegister,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/organization-login',
        name: 'organization.login',
        component: OrganizationLogin,
        meta: {
            auth: false
        }
    },
    // USER ROUTES
    {
        path: '/organization/dashboard',
        name: 'organization.dashboard',
        component: OrganizationDashboard,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/organization/dashboard/create',
        name: 'organization.create',
        component: OrganizationCreate,
        meta: {
            auth: undefined
        }
    },
    // ADMIN ROUTES
    // {
    //     path: '/admin',
    //     name: 'admin.dashboard',
    //     component: AdminDashboard,
    //     meta: {
    //         auth: {roles: 2, redirect: {name: 'login'}, forbiddenRedirect: '/403'}
    //     }
    // },
]

const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
})

export default router
